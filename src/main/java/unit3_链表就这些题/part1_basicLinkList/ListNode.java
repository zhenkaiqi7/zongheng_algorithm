package unit3_链表就这些题.part1_basicLinkList;

/**
 * 在算法中最常用的链表定义方式
 */
public class ListNode {
    public int val;
    public ListNode next;

    ListNode(int x) {
        val = x;
        next = null;
    }

    public static void main(String[] args) {
        ListNode listnode=new ListNode(1);
    }
}
